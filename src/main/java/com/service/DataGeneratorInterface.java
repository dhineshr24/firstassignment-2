package com.service;

public interface DataGeneratorInterface {

    void generate(int dataSize, String path);
    void copyFilesFromLocal(String hadoop,String localPath,String hdfsPath);

}
