package com.data.persons;

import com.opencsv.CSVWriter;
import com.service.DataGeneratorInterface;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;


public class PersonsDataGenerator implements DataGeneratorInterface {

    @Override
    public void generate(int dataSize, String outputPath) {

        for (int i = 0; i < dataSize; i++) {

            try {

                Person person = new Person();
                String fileName = "person_"+i+".csv";
                File dir = new File (outputPath);
                File file = new File (dir, fileName);

                FileWriter outputfile = new FileWriter(file);

                CSVWriter writer = new CSVWriter(outputfile);

                String[] header = {"Name", "Age", "Company", "Building_code", "Phone_number", "Address"};
                writer.writeNext(header);

                String[] data1 = {person.getName(), String.valueOf(person.getAge()),
                        person.getCompany(), String.valueOf(person.getBuilding_code()), person.getPhone_number(), person.getAddress()};
                writer.writeNext(data1);

                writer.close();

            } catch (IOException e) {
                e.printStackTrace();
            }

        }

    }

    @Override
    public void copyFilesFromLocal(String hadoop,String localPath,String hdfsPath) {

        Configuration conf = new Configuration();
        FileSystem fs = null;
        try {
            fs = FileSystem.get(conf);
            fs.copyFromLocalFile(new Path(localPath),
                    new Path(hdfsPath));
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
