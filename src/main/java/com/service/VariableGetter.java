package com.service;

import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.util.Bytes;

public class VariableGetter {

    private String HADOOP_HOME = "/opt/homebrew/Cellar/hadoop/3.3.0/libexec/etc/hadoop/";
    private String localPath = "/Users/devharsha/IdeaProjects/FirstAssignment/output";
    private String hdfsPath = "/user/DevHarsha/";

    private TableName TABLE_NAME = TableName.valueOf("person");
    private byte[] CF_NAME = Bytes.toBytes("information");



    private byte[] PERSON_NAME = Bytes.toBytes("name");
    private byte[] PERSON_AGE = Bytes.toBytes("age");
    private byte[] PERSON_COMPANY = Bytes.toBytes("company");
    private byte[] PERSON_BUILDING_CODE = Bytes.toBytes("building_code");
    private byte[] PERSON_PHONE_NUMBER = Bytes.toBytes("phone_number");
    private byte[] PERSON_ADDRESS = Bytes.toBytes("address");


    public String getLocalPath() {
        return localPath;
    }

    public String getHdfsPath() {
        return hdfsPath;
    }

    public String getHADOOP_HOME() {
        return HADOOP_HOME;
    }

    public TableName getTABLE_NAME() {
        return TABLE_NAME;
    }

    public byte[] getCF_NAME() {
        return CF_NAME;
    }

    public byte[] getPERSON_NAME() {
        return PERSON_NAME;
    }

    public byte[] getPERSON_AGE() {
        return PERSON_AGE;
    }

    public byte[] getPERSON_COMPANY() {
        return PERSON_COMPANY;
    }

    public byte[] getPERSON_BUILDING_CODE() {
        return PERSON_BUILDING_CODE;
    }

    public byte[] getPERSON_PHONE_NUMBER() {
        return PERSON_PHONE_NUMBER;
    }

    public byte[] getPERSON_ADDRESS() {
        return PERSON_ADDRESS;
    }
}
